export const throttle = (func, ms) => {
  var isThrottled = false,
    savedArgs,
    savedThis;

  function wrapper() {
    if (isThrottled) {
      savedArgs = arguments;
      savedThis = this;
      return;
    }

    func.apply(this, arguments);

    isThrottled = true;

    setTimeout(function () {
      isThrottled = false;
      if (savedArgs) {
        wrapper.apply(savedThis, savedArgs);
        savedArgs = savedThis = null;
      }
    }, ms);
  }

  return wrapper;
};

// RESP

export class Resp {
  /**
   * Get window's current width.
   *
   * @get
   * @static
   * @return {Number}
   */
  static get currWidth() {
    return window.innerWidth;
  }

  /**
   * Detect touch events.
   *
   * @get
   * @static
   * @return {Boolean}
   */
  static get isTouch() {
    return 'ontouchstart' in document.documentElement;
  }

  /**
   * Detect XL desktop device.
   *
   * @get
   * @static
   * @return {Boolean}
   */
  static get isXL() {
    return process.browser ? window.matchMedia(`(min-width: 1600px)`).matches : false;
  }

  /**
   * Detect All desktop device.
   *
   * @get
   * @static
   * @return {Boolean}
   */
  static get allDesk() {
    return process.browser ? window.matchMedia(`(min-width: 1280px)`).matches : false;
  }

  /**
   * Detect desktop device.
   *
   * @get
   * @static
   * @return {Boolean}
   */
  static get isDesktop() {
    return process.browser ? window.matchMedia(`(min-width: 1280px) and (max-width: 1599px)`).matches : false;
  }

  /**
   * Detect tablet device.
   *
   * @get
   * @static
   * @return {Boolean}
   */
  static get isTablet() {
    return process.browser ? window.matchMedia(`(min-width: 768px) and (max-width: 1279px)`).matches : false;
  }

  /**
   * Detect tablet device. (landscape)
   *
   * @get
   * @static
   * @return {Boolean}
   */
  static get isTabletL() {
    return process.browser ? window.matchMedia(`(min-width: 992px) and (max-width: 1229px)`).matches : false;
  }

  /**
   * Detect tablet device. (portrait)
   *
   * @get
   * @static
   * @return {Boolean}
   */
  static get isTabletP() {
    return process.browser ? window.matchMedia(`(min-width: 768px) and (max-width: 991px)`).matches : false;
  }

  /**
   * Detect mobile device.
   *
   * @get
   * @static
   * @return {Boolean}
   */
  static get isMobile() {
    return process.browser ? window.matchMedia(`(max-width: 767px)`).matches : false;
  }

  /**
   * Detect safari web-browser.
   *
   * @get
   * @static
   * @return {Boolean}
   */
  static get isSafari() {
    const ua = navigator.userAgent.toLowerCase();
    return !!window.safari || (ua.includes('safari') && !ua.includes('chrome'));
  }

  /**
   * Detect iOS device.
   *
   * @get
   * @static
   * @return {Boolean}
   */
  static get isIos() {
    return /iphone|ipod|ipad/i.test(navigator.userAgent);
  }

  /**
   * Detect iOS device on Chrome.
   *
   * @get
   * @static
   * @return {Boolean}
   */
  static get isIosChrome() {
    return /CriOS/i.test(navigator.userAgent) && Resp.isIos;
  }

  /**
   * Detect firefox web-browser.
   *
   * @get
   * @static
   * @return {Boolean}
   */
  static get isFirefox() {
    return typeof window.InstallTrigger !== 'undefined';
  }

  /**
   * Detect internet explorer web-browser.
   *
   * @get
   * @static
   * @return {Boolean}
   */
  static get isIE() {
    return navigator.userAgent.indexOf('MSIE') !== -1 || navigator.appVersion.indexOf('Trident/') > 0;
  }
}

export const capitalizeFirstLetter = function(string) {
  if ( string == '/' ) return 'Home';
  return string.charAt(1).toUpperCase() + string.slice(2);
}
