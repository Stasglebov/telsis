import {capitalizeFirstLetter} from '~/plugins/_helpers.js';

export default async ({store, route}) => {
  if (process.server) return;
  try {
    await store.dispatch('getPageData', route);
  } catch (error) {
    throw error
  }
}
