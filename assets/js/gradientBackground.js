export default class GradientBackground {
  constructor(containerClass, options) {
    const defaults = {
      firstColor: '#644298',
      secondColor: '#e22266'
    };
    const populated = Object.assign(defaults, options);
    for (const key in populated) {
      if (populated.hasOwnProperty(key)) {
        this[key] = populated[key]
      }
    }
    this.class = containerClass;
    this.parentElement = document.querySelector('.intro');
    this.container = document.querySelector(containerClass);
    this.canvas = {};
    this.canvas.elem = document.createElement('canvas');
    if (window.innerWidth > 1200) {
      this.onResize();
      this.init();
      this.initMouseMoveEvent();
    }
  }
  init() {
    this.xPosition = {
      value: 0
    };
    this.yPosition = {
      value: 0
    };
    window.devicePixelRatio > 1 ? this.initRetinaDisplay() : this.initDefaultDisplay();
    this.render();
  }
  initRetinaDisplay() {
    this.canvas.width = this.container.offsetWidth
    this.canvas.height = this.container.offsetHeight
    this.canvas.elem.width = this.canvas.width * 2
    this.canvas.elem.height = this.canvas.height * 2
    this.canvas.elem.style.width = `${this.container.offsetWidth}px`
    this.canvas.elem.style.height = `${this.container.offsetHeight}px`
    this.container.appendChild(this.canvas.elem)
    this.ctx = this.canvas.elem.getContext('2d')
    this.ctx.scale(2, 2)
  }
  initDefaultDisplay() {
    this.canvas.width = this.container.offsetWidth
    this.canvas.height = this.container.offsetHeight
    this.canvas.elem.width = this.canvas.width
    this.canvas.elem.height = this.canvas.height
    this.container.appendChild(this.canvas.elem)
    this.ctx = this.canvas.elem.getContext('2d')
  }
  render() {
    let _this = this;
    requestAnimationFrame(function () {
      _this.drawGradient
    });
    _this.ctx.clearRect(0, 0, _this.canvas.width, _this.canvas.height);
    _this.drawGradient();
  }
  drawGradient() {
    let _this = this;
    _this.grd = this.ctx.createLinearGradient(0 - _this.xPosition.value / 2, 0, this.canvas.width + _this.xPosition.value / 2, this.canvas.height);
    _this.grd.addColorStop(0, _this.firstColor);
    // _this.grd.addColorStop(_this.xPosition.value/_this.canvas.width, '#a3327f');
    _this.grd.addColorStop(1, _this.secondColor);
    _this.ctx.clearRect(0, 0, _this.canvas.width, _this.canvas.height);
    _this.ctx.fillStyle = _this.backgroundColor;
    _this.ctx.save();
    _this.ctx.beginPath();
    _this.ctx.fillStyle = _this.grd;
    _this.ctx.fillRect(0, 0, _this.canvas.width, _this.canvas.height);
    _this.ctx.closePath();
    _this.ctx.restore();
  }
  initMouseMoveEvent() {
    let _this = this;
    requestAnimationFrame(function () {
      _this.drawGradient
    });
    _this.parentElement.addEventListener('mousemove', function (e) {
      _this.xPosition.value = e.pageX;
      _this.yPosition.value = e.pageY;
      _this.drawGradient();
    });
  }
  onResize() {
    let _that = this;
    // change canvas size and reinit()
    if (window.innerWidth < 1020) return;
    window.addEventListener('resize', this.changeSize = () => {
      _that.canvas.width = _that.container.offsetWidth;
      _that.canvas.height = _that.container.offsetHeight;
      _that.canvas.elem.width = _that.canvas.width;
      _that.canvas.elem.height = _that.canvas.height;
      _that.init();
    });
  }
  removeEventListeners() {
    window.removeEventListener('resize', this.changeSize);
  }
}
