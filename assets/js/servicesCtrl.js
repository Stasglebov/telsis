export default class Services {
  
  constructor () {
    this.items = [...document.querySelectorAll('.services .js-service-btn')]
    this.popupContainer = document.querySelector('.popup-services');
    this.btnDefaultContainer = document.querySelector('.services .item-wrapper');
    this.closeBtns = [...document.querySelectorAll('.close-btn')];
    this.leftBtn = document.querySelector('.services .left');
    this.rightBtn = document.querySelector('.services .right');
    this.init();
    this.initCloseEvent();
    this.initInner();
  }
  
  init () {
    let _that = this
    for (let i = 0; i < this.items.length; i++) {
      this.items[i].addEventListener('click', function(e) {
        e.preventDefault()
        _that.popupContainer.classList.add('active');
        _that.btnDefaultContainer.classList.add('not-active');
        console.log(this)
        if ( this.classList.contains('enterpriseBtn') ) {
          document.querySelector('.enterpriseBlock').classList.add('active');
          document.querySelector('.operatorsBlock').classList.remove('active');
          
          // sidebars
          document.querySelector('.services .right').classList.remove('active')
          document.querySelector('.services .left').classList.add('active')
        } else {
          document.querySelector('.enterpriseBlock').classList.remove('active');
          document.querySelector('.operatorsBlock').classList.add('active');
          
          // sidebars
          document.querySelector('.services .left').classList.remove('active')
          document.querySelector('.services .right').classList.add('active')
          
        }
      })
    }
  }
  
  initCloseEvent() {
    let _that = this;
    for (let i = 0; i < this.closeBtns.length; i++) {
      this.closeBtns[i].addEventListener('click', (e) => {
        e.preventDefault();
        _that.popupContainer.classList.remove('active');
        _that.btnDefaultContainer.classList.remove('not-active');
      })
    }
  }
  
  initInner() {
    this.leftBtn.addEventListener('click', (e) => {
      e.preventDefault();
      
    })
  
  }
  
}