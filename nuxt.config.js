module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'Telsis',
    meta: [
      {charset: 'utf-8'},
      {name: 'viewport', content: 'width=device-width, initial-scale=1'},
      {hid: 'description', name: 'description', content: 'Telsis'}
    ],
    link: [
      {rel: 'apple-touch-icon', sizes: '180x180', href: 'img/favicon/apple-touch-icon.png'},
      {rel: 'icon', type: 'image/png', sizes: '32x32', href: 'img/favicon/favicon-32x32.png'},
      {rel: 'icon', type: 'image/png', sizes: '16x16', href: 'img/favicon/favicon-16x16.png'},
      {rel: 'manifest', href: 'img/favicon/site.webmanifest',},
      {rel: 'mask-icon', href: 'img/favicon/safari-pinned-tab.svg', color: '#5bbad5'}
    ]
  },

  /*
  ** Generate rules
   */
  generate: {
    routes: function () {
      let routes = [];
      // const locales = [''];
      const pages = [
        '',
        'solutions'
      ];
      pages.map((page) => {
        routes.push(`/${page}`);
      });
      return routes;
    }
  },
  /*
  ** Add global styles
   */
  css: [
    {src: '@/assets/sass/styles.sass', lang: ['sass', 'scss']},
  ],

  /*
  ** Customize the progress bar color
  */
  loading: {
    color: '#644296',
    height: '4px'
  },
  loadingIndicator: {
    name: 'circle',
    color: '#3B8070',
    background: 'white'
  },

  plugins: [
    '~/plugins/globalComponents.js',
    '~/plugins/vee-validate.js',
    {
      src: '~/plugins/vue-flickity',
      ssr: false
    }
  ],
  /*
  ** Build configuration
  */
  modules: [
    '@nuxtjs/axios'
  ],
  router: {
    scrollBehavior: function (to, from, savedPosition) {
      return { x: 0, y: 0 }
    },
    middleware: 'getPageData'
  },
  build: {
    vendor: [
      //polifills
      // 'core-js/fn/object/assign',
      // 'core-js/fn/string/starts-with',
      // 'core-js/fn/string/includes',
      // 'core-js/fn/array/includes',
      // 'core-js/fn/array/find',
      //plugins
      'gsap',
      // 'aos',
      'gsap/ScrollToPlugin',
      'vee-validate',
      // '~/plugins/DrawSVGPlugin.js',
      // 'object-fit-videos',
      'object-fit-images',
      // 'scrollmagic',
      'intersection-observer',
    ],


    /*
    ** Run ESLint on save
    */
    extend(config, {isDev, isClient}) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        });
      }
      ;

      if (isClient) {
        config.resolve.alias['TweenMax'] = 'gsap/TweenMax';
        config.resolve.alias['TimelineMax'] = 'gsap/TimelineMax';
        config.resolve.alias['ScrollMagic'] = 'scrollmagic/scrollmagic/uncompressed/ScrollMagic';
        config.resolve.alias['ScrollMagicGSAP'] = 'scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap';
        config.resolve.alias['debug.addIndicators'] = 'scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators';
      }
      ;

      const urlLoader = config.module.rules.find((rule) => rule.loader === 'url-loader');
      urlLoader.test = /\.(png|jpe?g|gif)$/;

      /*
      ** SVG to component plugin config
       */
      config.module.rules.push({
        test: /\.svg$/,
        loader: 'vue-svg-loader',
        options: {
          svgo: {
            plugins: [
              {removeDoctype: true},
              {removeComments: true}
            ]
          }
        }
      });

    }
  }
}
