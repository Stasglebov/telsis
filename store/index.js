import {capitalizeFirstLetter} from '~/plugins/_helpers.js';

export const state = () => {
  pageData: null;
  globalData: null;
  showPreloader: true;
};

export const mutations = {
  SET_GLOBAL_DATA(state, data) {
    state.globalData = data
  },
  SET_PAGE_DATA(state, {data, receivedPage}) {
    if (!state.pageData) {
      state.pageData = {};
      state.pageData[receivedPage] = {};
    }
    state.pageData[receivedPage] = data;
  },
  UNLOCK_PAGE(state) {
    state.showPreloader = false;
  }
};

export const actions = {
  async getPageData({state, commit}, route) {
    let data;
    let path = capitalizeFirstLetter(route.path);
    let receivedPage = `page${path}`;
    if (state.pageData[receivedPage]) return;
    ({data} = await this.$axios.get(`https://telsis-11edd.firebaseio.com/pageData/page${path}/.json`));
    commit('SET_PAGE_DATA', {data, receivedPage});
  },

  async nuxtServerInit({state, commit}, context) {
    state.showPreloader = true;
    let data;
    let path = capitalizeFirstLetter(context.route.fullPath);
    ({data} = await this.$axios.get('https://telsis-11edd.firebaseio.com/globalData/.json'));
    commit('SET_GLOBAL_DATA', data);

    ({data} = await this.$axios.get(`https://telsis-11edd.firebaseio.com/pageData/page${path}/.json`));
    let receivedPage = `page${path}`;
    commit('SET_PAGE_DATA', {data, receivedPage});
  }
};






